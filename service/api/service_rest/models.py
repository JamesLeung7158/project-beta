from django.db import models
# Create your models here.

class AutomobileVO(models.Model):
  vin = models.CharField(max_length=200, unique=True)


class CustomerVO(models.Model):
  first_name = models.CharField(max_length=200)
  last_name = models.CharField(max_length=200)


class Technician(models.Model):
  first_name = models.CharField(max_length=200)
  last_name = models.CharField(max_length=200)
  employee_id = models.CharField(max_length=200, unique=True)


class Appointment(models.Model):
  date_time = models.DateTimeField()
  reason = models.CharField(max_length=200)
  status = models.CharField(max_length=20, default="created")
  vin = models.CharField(max_length=200)
  vip = models.BooleanField(default=False)
  customer = models.ForeignKey(
    CustomerVO,
    related_name='appointments',
    on_delete=models.PROTECT,
  )
  technician = models.ForeignKey(
    Technician,
    related_name='appointments',
    on_delete=models.PROTECT,
  )

  def canceled(self):
    self.status = "canceled"
    self.save()

  def finished(self):
    self.status = "finished"
    self.save()

  def isVIP(self):
    self.vip = True
    self.save()
