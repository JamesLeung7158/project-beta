from django.urls import path
from .views import (
    api_list_technicians,
    api_delete_technician,
    api_list_appointments,
    api_delete_appointment,
    api_appointment_canceled,
    api_appointment_finished,
    api_appointments_by_vin
)

urlpatterns = [
  path("technicians/", api_list_technicians, name="api_list_technicians"),
  path("technicians/<int:id>/", api_delete_technician, name="api_delete_technician"),
  path("appointments/", api_list_appointments, name="api_list_appointments"),
  path("appointments/<int:id>", api_delete_appointment, name="api_delete_appointment"),
  path("appointments/<int:id>/cancel", api_appointment_canceled, name="api_appointment_canceled"),
  path("appointments/<int:id>/finish", api_appointment_finished, name="api_appointment_finished"),
  path("appointments/vin/<str:vin>", api_appointments_by_vin, name="api_appointments_by_vin"),
]
