import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';


function ModelForm () {
  const [model, setModel] = useState('');
  const handleModelChange = (event) => {
    const value = event.target.value;
    setModel(value);
  }

  const [modelPicture, setModelPicture] = useState('');
	const handleModelPictureChange = (event) => {
		const value = event.target.value;
		setModelPicture(value);
	}

  const [modelManufacturer, setModelManufacturer] = useState('');
	const handleModelManufacturerChange = (event) => {
		const value = event.target.value;
		setModelManufacturer(value);
	}

  const [manufacturers, setManufacturers] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/'

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }

  useEffect(() => {
    fetchData();
  }, [])

  const navigateTo = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = model;
    data.picture_url = modelPicture;
    data.manufacturer_id = modelManufacturer;

    const modelUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json'
      }
    }

    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      navigateTo('/models')
    }
  }

  return (
    <div>
      <div className="my-5 container">
        <Link
        to={
          '..'
        }
        >
          <button className="btn bg-2" id="goBackButton">Back to model list</button>
        </Link>
      </div>
      <div className="shadow p-4 mt-4">
        <h1>Add a new model</h1>
        <form onSubmit={handleSubmit} >
          <div className="form-floating mb-3">
            <input
            onChange={handleModelChange}
            placeholder='model'
            type='text'
            value={model}
            className="form-control"
            />
            <label htmlFor='model'>Model</label>
          </div>
          <div className="form-floating mb-3">
            <input
            onChange={handleModelPictureChange}
            placeholder='model picture url'
            type='text'
            value={modelPicture}
            className="form-control"
            />
            <label htmlFor='modelPicture'>Model Picture URL</label>
          </div>
          <div className="form-floating mb-3">
            <select
            onChange={handleModelManufacturerChange}
            placeholder='model manufacturer'
            type='text'
            value={modelManufacturer}
            className="form-select"
            >
            <option value=''>Select manufacturer</option>
              {manufacturers.map(manufacturer => {
                return (
                  <option key={manufacturer.name} value={manufacturer.id}>
                    {manufacturer.name}
                  </option>
                )
              })}
              </select>
          </div>
          <button className="btn bg-2" id="newThingButton">Add model</button>
        </form>
      </div>
    </div>
  )
}

export default ModelForm;
