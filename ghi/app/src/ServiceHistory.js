import React, { useEffect, useState } from 'react';


function ServiceHistory () {

  const [vin, setVin] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    // get the new data ,
    // filter the data to only have those matching the submitted vin
    // update state
    const fetchConfig = {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
      },
    };
    if (vin === '') {
      const url = `http://localhost:8080/api/appointments`
      const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    }
    } else {
      const url = `http://localhost:8080/api/appointments/vin/${vin}`
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments)
      }
    }
  }

  return (
    <div>
      <div className="my-5 container">
        <h1>Service History</h1>
        <div className="form-floating mb-3">
          <form onSubmit={handleSubmit}>
            <input onChange={handleVinChange} placeholder='Search by VIN' type='text' name='vin' className="form-control"/>
            <button className="btn bg-2" id="newThingButton">Search</button>
          </form>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Is VIP?</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {appointments.map((appointment) => {
              let datetime = appointment.date_time;
              datetime = new Date(datetime);
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{String(appointment.vip)}</td>
                  <td>{appointment.customer.first_name} {appointment.customer.last_name}</td>
                  <td>{datetime.toLocaleDateString()}</td>
                  <td>{datetime.toLocaleTimeString()}</td>
                  <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                  <td>{appointment.reason}</td>
                  <td>{appointment.status}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default ServiceHistory;
