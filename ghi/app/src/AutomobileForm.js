import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';


function AutomobileForm () {
  const [automobile, setAutomobile] = useState({
    color: '',
    year: '',
    vin: '',
    model_id: '',
  });

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setAutomobile({
      ...automobile,
      [inputName]: value
  });
  }

  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(automobile),
      headers: {
        'Content-type': 'application/json'
      }
    }

    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      setAutomobile({
        color: '',
        year: '',
        vin: '',
        model_id: '',
      });
      navigate('/automobiles')
    }
  }


  return (
    <div>
      <div className="my-5 container">
        <Link
        to={
          '..'
        }
        >
          <button className="btn bg-2" id="goBackButton">Back to automobile list</button>
        </Link>
      </div>
      <div className="shadow p-4 mt-4">
        <h1>Add a new automobile</h1>
        <form onSubmit={handleSubmit} >
          <div className="form-floating mb-3">
            <input
            onChange={handleAutomobileChange}
            placeholder='color'
            type='text'
            name='color'
            className="form-control"
            />
            <label htmlFor='color'>Color</label>
          </div>
          <div className="form-floating mb-3">
            <input
            onChange={handleAutomobileChange}
            placeholder='year'
            type='text'
            name='year'
            className="form-control"
            />
            <label htmlFor='year'>Year</label>
          </div>
          <div className="form-floating mb-3">
            <input
            onChange={handleAutomobileChange}
            placeholder='vin (max 17 characters)'
            type='text'
            name='vin'
            className="form-control"
            />
            <label htmlFor='vin'>VIN</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleAutomobileChange}
              placeholder='model_id'
              type='text'
              name='model_id'
              className="form-control"
              />
              <label htmlFor='model_id'>Model ID</label>
          </div>
          <button className="btn bg-2" id="newThingButton">Add</button>
        </form>
      </div>
    </div>
  )
}

export default AutomobileForm;
