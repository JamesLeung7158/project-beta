import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';


function TechnicianForm () {

  const [technician, setTechnician] = useState({
    employee_id: '',
    first_name: '',
    last_name: '',
  })

  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setTechnician({
      ...technician,
      [inputName]: value
    });
  }

  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const technicianUrl = 'http://localhost:8080/api/technicians/'
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(technician),
      headers: {
        'Content-type': 'application/json'
      }
    }

    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      setTechnician({
        employee_id: '',
        first_name: '',
        last_name: '',
      })
      navigate('/technicians')
    }
  }

  return (
    <div>
      <div className="my-5 container">
        <Link to={'..'}>
          <button className="btn bg-2" id="goBackButton">Back to technician list</button>
        </Link>
      </div>
      <div className="shadow p-4 mt-4">
        <h1>Add a technician</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input
            onChange={handleTechnicianChange}
            placeholder='Employee ID'
            type='text'
            name='employee_id'
            className="form-control"
            />
            <label htmlFor='employee_id'>employee id</label>
          </div>
          <div className="form-floating mb-3">
            <input
            onChange={handleTechnicianChange}
            placeholder='First Name'
            type='text'
            name='first_name'
            className="form-control"
            />
            <label htmlFor='first_name'>first name</label>
          </div>
          <div className="form-floating mb-3">
            <input
            onChange={handleTechnicianChange}
            placeholder='Last Name'
            type='text'
            name='last_name'
            className="form-control"
            />
            <label htmlFor='last_name'>last name</label>
          </div>
          <button className="btn bg-2" id="newThingButton">Add</button>
        </form>
      </div>
    </div>
  )

}

export default TechnicianForm;
