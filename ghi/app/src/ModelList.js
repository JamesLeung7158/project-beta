import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function ModelList () {
  const [models, setModels] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
    // maybe add error page
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div className="my-5 container">
        <Link
        to={{
          pathname: 'new'
        }}
        >
          <button className="btn bg-2" id="newThingButton">Add a vehicle model</button>
        </Link>
      </div>
      <div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Vehicle Manufacturer</th>
              <th>Vehicle Model</th>
              <th>Vehicle Picture</th>
            </tr>
          </thead>
          <tbody>
            {models.map((model) => {
              return (
                <tr key={model.id}>
                  <td>{model.manufacturer.name}</td>
                  <td>{model.name}</td>
                  <td>
                    <img
                    src={model.picture_url}
                    alt={model.name}
                    style={{
                      width: "200px",
                      height: "150px"
                    }}
                    />
                  </td>
                </tr>
              )
            })
            }
          </tbody>
        </table>
      </div>
    </div>
  )
};

export default ModelList;
