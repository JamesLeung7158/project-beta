import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';

function SalesList () {
  const [sales, setSales] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/sales/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (event) => {
    document.getElementById("delete-message").innerHTML = `Deleted sale of VIN: ${event[2]}`
    document.getElementById("removeSuccessMessage").classList.remove('d-none')
    document.getElementById("delete-message").classList.remove('d-none')
    const sale_href = event[0]
    const auto_href = event[1]

    const saleUrl = `http://localhost:8090${sale_href}`
    const fetchConfig = {
      method: 'delete',
    }

    const autoUrl = `http://localhost:8100${auto_href}unsold`
    const fetchAutoConfig = {
      method: 'put',
      headers: {
        'Content-type': 'application/json'
      }
    }

    const autoResponse = await fetch(autoUrl, fetchAutoConfig);
    if (autoResponse.ok) {
      const updatedAuto = await autoResponse.json()
    }

    const response = await fetch(saleUrl, fetchConfig);
    if (response.ok) {
      fetchData();
    }
    
  }

  const removeSuccessMessage = async (event) => {
    event.preventDefault();
    document.getElementById("delete-message").classList.add('d-none')
    document.getElementById("removeSuccessMessage").classList.add('d-none')

  }
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [modalData, setModalData] = useState(null);
  function openModal() {

    setModalIsOpen(true);
  }
  function closeModal() {
    setModalIsOpen(false);
  }

  return (
    <div>
      <div className="my-0 container">
        <Link
        to={{
          pathname: 'new'
        }}
        >
          <button className="btn bg-2" id="newThingButton">Record a new sale</button>
        </Link>
      </div>
      <div className="alert alert-success mb-0 d-none" id="delete-message">
      </div>
      <div className="d-none" id="removeSuccessMessage">
        <button onClick={removeSuccessMessage} className="btn bg-2">Remove success message</button>
      </div>
      <div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Employee ID</th>
              <th>Salesperson Name</th>
              <th>Customer</th>
              <th>Automobile VIN</th>
              <th>Price</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {sales.map((sale) => {
              return (
                <tr key={sale.href}>
                  <td>{sale.salesperson.employee_id}</td>
                  <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                  <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>${sale.price}</td>
                  <td>
                    <button 
                    onClick={() => {setModalIsOpen(true); setModalData(sale)}}
                    className="btn btn-danger"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              )
            })
            }
          </tbody>
          <Modal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          contentLabel="Delete Confirm Modal"
          ariaHideApp={false}
          >
            
            <p className="mt-3"> Are you sure you want to delete this sale of VIN: {modalData?.automobile?.vin}?</p>
            <div>
              <button 
              onClick={() => {handleDelete([modalData?.href, modalData?.automobile?.import_href, modalData?.automobile?.vin]); setModalIsOpen(false)}}
              className="btn btn-danger mx-3"
              >
                Yes
              </button> 
              <button onClick={closeModal} className="btn bg-2 mx-3">No</button>
            </div>
          </Modal>
        </table>
      </div>
    </div>
  )
};

export default SalesList;
