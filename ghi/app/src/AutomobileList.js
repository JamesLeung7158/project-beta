import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function AutomobileList () {
	const [automobiles, setAutomobiles] = useState([]);

	const fetchData = async () => {
		const url = 'http://localhost:8100/api/automobiles/'

		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setAutomobiles(data.autos)
		}
	}

	useEffect(() => {
			fetchData();
		}, []);

	return (
		<div>
			<div className="my-5 container">
				<Link
				to={{
					pathname: 'new'
				}}
				>
					<button className="btn bg-2" id="newThingButton">Add an automobile</button>
				</Link>
			</div>
			<div>
			<table className="table table-striped">
					<thead>
						<tr>
							<th>Sold</th>
							<th>Model</th>
							<th>Color</th>
							<th>VIN</th>
						</tr>
					</thead>
					<tbody>
					{automobiles.map((automobile) => {
						return (
						<tr key={automobile.id}>
							<td>{String(automobile.sold)}</td>
							<td>{automobile.year}</td>
							<td>{automobile.model.name}</td>
							<td>{automobile.color}</td>
							<td>{automobile.vin}</td>
						</tr>
						)
					})
					}
					</tbody>
			</table>
			</div>
		</div>
	)
}

export default AutomobileList;
