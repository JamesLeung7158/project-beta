import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

function CustomerForm () {
  const [firstname, setFirstname] = useState('');
  const handleFirstnameChange = (event) => {
    const value = event.target.value;
    setFirstname(value);
  }

  const [lastname, setLastname] = useState('');
  const handleLastnameChange = (event) => {
    const value = event.target.value;
    setLastname(value);
  }

  const [phonenumber, setPhonenumber] = useState('');
  const handlePhonenumberChange = (event) => {
    const value = event.target.value;
    setPhonenumber(value);
  }

  const [address, setAddress] = useState('');
  const handleAddressChange = (event) => {
    const value = event.target.value;
    setAddress(value);
  }

  const navigateTo = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.first_name = firstname;
    data.last_name = lastname;
    data.phone_number = phonenumber;
    data.address = address

    const customerUrl = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json'
      }
    }

    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      navigateTo('/customers')
    }
  }

  return (
    <div>
      <div className="my-5 container">
        <Link
        to={
          '..'
        }
        >
          <button className="btn bg-2" id="goBackButton">Back to customer list</button>
        </Link>
      </div>
      <div className="shadow p-4 mt-4">
        <h1>Add a new customer</h1>
        <form onSubmit={handleSubmit} >
          <div className="form-floating mb-3">
            <input
            onChange={handleFirstnameChange}
            placeholder='first name'
            type='text'
            value={firstname}
            className="form-control"
            />
            <label htmlFor='firstname'>First Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
            onChange={handleLastnameChange}
            placeholder='last name'
            type='text'
            value={lastname}
            className="form-control"
            />
            <label htmlFor='lastname'>Last Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
            onChange={handlePhonenumberChange}
            placeholder='phone number'
            type='text'
            value={phonenumber}
            className="form-control"
            />
            <label htmlFor='phone number'>Phone Number</label>
          </div>
          <div className="form-floating mb-3">
            <input
            onChange={handleAddressChange}
            placeholder='address'
            type='text'
            value={address}
            className="form-control"
            />
            <label htmlFor='address'>Address</label>
          </div>
          <button className="btn bg-2" id="newThingButton">Add customer</button>
        </form>
      </div>
    </div>
  )
};

export default CustomerForm;
