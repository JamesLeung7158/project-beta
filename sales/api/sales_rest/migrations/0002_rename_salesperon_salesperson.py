# Generated by Django 4.0.3 on 2023-04-25 01:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Salesperon',
            new_name='Salesperson',
        ),
    ]
